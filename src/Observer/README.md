# Observer
Toteuta luentomateriaalissa (Observer.pdf) Observer-mallin sovelluksena esitetty hahmotelma digitaalisesta kellosta Javalla täydentäen koodia puuttuvin osin. Käytä ratkaisussasi Javan APIsta löytyviä Observer-rajapintaa ja Observable-luokkaa.
