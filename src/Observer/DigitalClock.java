package Observer;

import java.util.Observable;
import java.util.Observer;

public class DigitalClock implements Observer {
	
	private ClockTimer timer;
	
	public DigitalClock(ClockTimer ct) {
		timer = ct;
		timer.attach(this);
	}
	
	public void draw() {
		String date = timer.getDate();
		System.out.println(date);
	}

	public void update(Observable o, Object arg) {
		draw();
	}
}