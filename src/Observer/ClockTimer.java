package Observer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ClockTimer extends Subject {
	
	public ClockTimer() {
		Timer timer = new Timer();
		timer.schedule(new Ticker(), 0, 1 * 1000);
	}
	
	public String getDate() {
		Date now = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm:ss a 'on' EEEE, MMMM d, yyyy");
	    String date = dateFormatter.format(now);
	    return date;
	}
	
	class Ticker extends TimerTask {
		@Override
		public void run() {
			notifier();
		}
	}
}
