# Iterator
## Tehtävänanto
1. Tutki kuinka Javan iteraattori käyttäytyy, jos yritetään iteroida kokoelmaa kahdella säikeellä yhtä aikaa, kun molemmilla on oma iterattori.
2. Entä jos säikeet käyttävät samaa iteraattoria vuorotellen?
3. Kuinka käy, jos kokoelmaan tehdään muutoksia iteroinnin läpikäynnin aikana?
4. Keksi jotain muuta testattavaa.

## Vastaukset
_Kokoelmana toimii 21 -alkioinen ArrayList, jossa on kokonaislukuja_
1. Homma toimii niinkuin pitääkin ja säikeet kulkevat kokoelman läpi järjestyksessä. Tämä johtuu siitä, että kun molemmilla säikeillä on oma iteraattori, molemmat säikeet myös tietävät missä mennään, eikä toisen säikeen toimet vaikuta toiseen. Alla ohjelman tulostus.

    ```
    thread 1: 0
    thread 2: 0
    thread 1: 1
    thread 2: 1
    thread 1: 2
    thread 2: 2
    thread 1: 3
    thread 2: 3
    thread 1: 4
    thread 2: 4
    thread 1: 5
    thread 2: 5
    thread 1: 6
    thread 2: 6
    thread 1: 7
    thread 2: 7
    thread 1: 8
    thread 2: 8
    thread 1: 9
    thread 2: 9
    thread 1: 10
    thread 2: 10
    thread 1: 11
    thread 2: 11
    thread 1: 12
    thread 2: 12
    thread 1: 13
    thread 2: 13
    thread 1: 14
    thread 2: 14
    thread 1: 15
    thread 2: 15
    thread 1: 16
    thread 2: 16
    thread 1: 17
    thread 2: 17
    thread 1: 18
    thread 2: 18
    thread 1: 19
    thread 2: 19
    thread 1: 20
    thread 2: 20
    ```

2. Tässä homma menee pahasti pieleen, sillä kun ensimmäinen säie kutsuu `iterator.Next()`, myös toisen säikeen osoitin siirtyy eteenpäin. Alla ohjelman tulostus.

    ```
    thread 1: 0
    thread 2: 1
    thread 1: 2
    thread 2: 3
    thread 1: 4
    thread 2: 5
    thread 1: 6
    thread 2: 7
    thread 1: 8
    thread 2: 9
    thread 1: 10
    thread 2: 11
    thread 1: 12
    thread 2: 13
    thread 1: 14
    thread 2: 15
    thread 1: 16
    thread 2: 17
    thread 1: 18
    thread 2: 19
    thread 1: 20
    ```
3. Listan muuttaminen kesken ajon näkyy myös iteraattorien läpikäynneissä. Muutin kokoelman viimeisen alion `20 -> 99`. Tuntuu pelittävän niinkuin pitääkin. Alla ohjelman tulostus.

    ```
    thread 1: 0
    thread 2: 0
    thread 1: 1
    thread 2: 1
    thread 1: 2
    thread 2: 2
    thread 1: 3
    thread 2: 3
    thread 1: 4
    thread 2: 4
    thread 1: 5
    thread 2: 5
    thread 1: 6
    thread 2: 6
    thread 1: 7
    thread 2: 7
    thread 1: 8
    thread 2: 8
    thread 1: 9
    thread 2: 9
    thread 1: 10
    thread 2: 10
    thread 1: 11
    thread 2: 11
    thread 1: 12
    thread 2: 12
    thread 1: 13
    thread 2: 13
    thread 1: 14
    thread 2: 14
    thread 1: 15
    thread 2: 15
    thread 1: 16
    thread 2: 16
    thread 1: 17
    thread 2: 17
    thread 1: 18
    thread 2: 18
    thread 1: 19
    thread 2: 19
    thread 1: 99
    thread 2: 99
    ```

4. Valitettavasti en nyt keksinyt enää muuta testattavaa ☹️
