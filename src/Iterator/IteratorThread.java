package Iterator;

import java.util.List;
import java.util.ListIterator;

public class IteratorThread extends Thread {
	String name;
	List<Integer> list;
	ListIterator<Integer> commonIterator;
	ListIterator<Integer> myIterator;
	Boolean hasNext;
	SynchronizedIterator synchronizedIterator;
	
	public IteratorThread(String name, List<Integer> list, ListIterator<Integer> commonIterator, SynchronizedIterator synchronizedIterator) {
		this.name = name;
		this.list = list;
		this.commonIterator = commonIterator;
		this.synchronizedIterator = synchronizedIterator;
		myIterator = list.listIterator();
	}
	
	public void iterateWithCommonIterator() {
		synchronizedIterator.iterate(commonIterator, name);
	}
	
	public void iterateWithMyIterator() {
		synchronizedIterator.iterate(myIterator, name);
	}
	
	public void run() {
		iterateWithMyIterator();
	}
}