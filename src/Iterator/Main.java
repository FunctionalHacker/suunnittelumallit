package Iterator;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		for(int i = 0; i <= 20; i++) {
			list.add(i);
		}
		SynchronizedIterator synchronizedIterator = new SynchronizedIterator();
		ListIterator<Integer> commonIterator = list.listIterator();
		IteratorThread thread1 = new IteratorThread("thread 1", list, commonIterator, synchronizedIterator);
		IteratorThread thread2 = new IteratorThread("thread 2", list, commonIterator, synchronizedIterator);
		IteratorThread thread3 = new IteratorThread("thread 3", list, commonIterator, synchronizedIterator);
		thread1.start();
		list.set(20, 99);
		thread2.start();
		list.add(150);
		thread3.start();
	}
}