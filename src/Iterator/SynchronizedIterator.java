package Iterator;

import java.util.ListIterator;

public class SynchronizedIterator {
	public synchronized void iterate(ListIterator<Integer> iterator, String threadName) {
		while(iterator.hasNext()) {
			System.out.println(threadName+": "+iterator.next());
			try {
				wait(100);
				notifyAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
