package StateAndSingleton;

public class Pokemon {
	private State state;
	public Pokemon() {
		setState(Charmander.getInstance());
	}
	void setState(final State newState) {
		state = newState;
	}
	
	public void evolve() {
		state.evolve(this);
	}
	
	public void printStage() {
		state.printStage();
	}
}
