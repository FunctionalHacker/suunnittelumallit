package StateAndSingleton;

public class Charmeleon implements State {
	
	private static final Charmeleon instance = new Charmeleon();
	public static State getInstance() {
		return instance;
	}
	
	private Charmeleon() {}
	public void evolve(final Pokemon pokemon) {
		pokemon.setState(Charizard.getInstance());
	}

	public void printStage() {
		System.out.println("I am Charmeleon!");
	}
}
