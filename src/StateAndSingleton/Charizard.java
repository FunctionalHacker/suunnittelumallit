package StateAndSingleton;

public class Charizard implements State {
	
	private static final Charizard instance = new Charizard();
	public static State getInstance() {
		return instance;
	}
	
	private Charizard() {}
	public void evolve(final Pokemon pokemon) {}

	public void printStage() {
		System.out.println("I am Charizard!");
	}
}
