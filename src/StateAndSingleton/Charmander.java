package StateAndSingleton;

public class Charmander implements State {
	
	private static final Charmander instance = new Charmander();
	public static State getInstance() {
		return instance;
	}
	
	public void evolve(final Pokemon pokemon) {
		pokemon.setState(Charmeleon.getInstance());
	}
	
	public void printStage() {
		System.out.println("I am Charmander!");
	}
}
