package StateAndSingleton;

public class Main {
    public static void main(String[] args) {
    	final Pokemon pokemon = new Pokemon();
    	
    	for(int i = 1; i <= 3; i++) {
    		pokemon.printStage();
    		pokemon.evolve();
    	}
    }
}