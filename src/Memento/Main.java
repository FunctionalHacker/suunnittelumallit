package Memento;

public class Main {
	public static void main(String[] args) {
		int high = 100;
		int low = 1;
		Riddler riddler = new Riddler(high, low);
		Client antti = new Client(riddler, "Antti", high, low);
		Client pekka = new Client(riddler, "Pekka", high, low);
		Client marko = new Client(riddler, "Marko", high, low);
		Client mikko = new Client(riddler, "Mikko", high, low);
		
		new Thread(antti).start();
		new Thread(pekka).start();
		new Thread(marko).start();
		new Thread(mikko).start();
	}
}