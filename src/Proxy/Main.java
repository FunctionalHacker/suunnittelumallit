package Proxy;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public static void main(final String[] arguments) {
		Scanner sc = new Scanner(System.in);
		final Image image1 = new ProxyImage("image1.jpg");
		final Image image2 = new ProxyImage("image2.jpg");
		final Image image3 = new ProxyImage("image3.jpg");
		final Image image4 = new ProxyImage("image4.jpg");
		
		ArrayList<Image> images = new ArrayList<Image>();
		images.add(image1);
		images.add(image2);
		images.add(image3);
		images.add(image4);
		
		for (Image image : images) {
			image.showData();
		}
		
		
		int currentImage = 0;
		String selection;
		System.out.println(" ");
		images.get(currentImage).displayImage();
		while(true) {
			System.out.println(" ");
			System.out.print("forward [f] or backward [b]: ");
			selection = sc.next();
			if(selection.equals("f")) {
				if(currentImage == images.size() - 1) {
					currentImage = 0;
				}
				else {
					currentImage++;
				}
			}
			if(selection.equals("b")) {
				if(currentImage == 0) {
					currentImage = images.size() - 1;
				}
				else {
					currentImage--;
				}
			}
			System.out.println(" ");
			images.get(currentImage).displayImage();
		}
	}
}