# Strategy
Tee a tai b
**Tein tehtävän A**

**a)** Määrittele Strategy-mallin mukaisesti ListConverter-rajapinta, jossa määritellään listToString-metodi, joka saa parametrikseen listatietorakenteen ja palauttaa listan merkkijonona, jossa
1. Strategia kirjoittaa rivinvaihtomerkin jokaisen alkion jälkeen
2. Strategia tulostaa rivinvaihtomerkin joka toisen alkion jälkeen
3. Strategia tulostaa rivinvaihtomerkin joka kolmannen alkion jälkeen
Toteuta eri strategioissa listan läpikäynti eri tavoin.

Eri tapoja:
- Iteraattorin käyttö
- Lista taulukoksi ennen läpikäyntiä, joka toteutetaan for-silmukassa
- Listan läpikäynti for-silmukassa käyttäen List-rajapinnan get-metodia.

Testiohjelmassa luodaan lista ja tulostetaan eri strategioilla aikaansaatu
lopputulos.

**b)** Toteuta vähintään kolme erilaista taulukon lajittelualgoritmia
(lähdekoodeja esim. http://www.cs.ubc.ca/~harrison/Java/sortingdemo.html). Esitä sovellus, jossa lajittelualgoritmin valinta toteutetaan
Strategy-mallin mukaisesti. Generoi tarpeeksi suuri lajiteltava aineisto ja
mittaa lajitteluun kuluvat ajat.

