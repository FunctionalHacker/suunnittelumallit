package Strategy;

import java.util.List;

public class ListPrinter {
	private ListConverter converter;
	
	public ListPrinter(ListConverter converter) {
		this.converter = converter;
	}
	
	public void setConverter(ListConverter converter) {
		this.converter = converter;
	}
	
	public void print(List<String> strings) {
		System.out.println(converter.listToString(strings));
	}
}
