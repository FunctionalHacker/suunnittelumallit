package Strategy;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		ListConverter ec = new EveryCell();
		ListConverter eoc = new EveryOtherCell();
		ListConverter etc = new EveryThirdCell();
		ListPrinter printer = new ListPrinter(ec);
		List<String> strings = new ArrayList<String>();
		strings.add("Linux");
		strings.add("is");
		strings.add("better");
		strings.add("than");
		strings.add("Windows");
		strings.add("admit");
		strings.add("it");
		
		printer.print(strings);
		printer.setConverter(eoc);
		printer.print(strings);
		printer.setConverter(etc);
		printer.print(strings);
	}
}
