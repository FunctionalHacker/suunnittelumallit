package Strategy;

import java.util.List;

public class EveryCell implements ListConverter {
	private String result;
	private int counter;

	public String listToString(List<String> list) {
		counter = 0;
		result = null;
		for (String cell : list) {
			if(counter == 0) {
				result = cell + "\n";
			}
			else {
				result = result + cell + "\n";
			}
			counter++;
		}
		return result;
	}
}
