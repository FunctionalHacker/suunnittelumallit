package Visitor;

public class Charizard implements State {
	
	private int bonusPoints;
	public int getBonusPoints() {
		return bonusPoints;
	}
	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}
	
	private Charizard() {}
	private static final Charizard instance = new Charizard();
	public static State getInstance() {
		return instance;
	}
	
	public void evolve(final Pokemon pokemon) {}

	public void printStage() {
		System.out.println("I am Charizard and I have "+bonusPoints+" bonus points!");
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}	
}
