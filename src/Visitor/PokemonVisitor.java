package Visitor;

public class PokemonVisitor implements Visitor {

	public void visit(Charmander state) {
		state.setBonusPoints(1);
	}

	public void visit(Charmeleon state) {
		state.setBonusPoints(2);
	}

	public void visit(Charizard state) {
		state.setBonusPoints(3);
	}
}
