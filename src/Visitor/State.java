package Visitor;

public interface State {
	public void setBonusPoints(int bonusPoints);
	public int getBonusPoints();
	public void evolve(final Pokemon pokemon);
	public void printStage();
	public void accept(Visitor visitor);
}
