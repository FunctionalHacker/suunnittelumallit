package Visitor;

public class Main {
    public static void main(String[] args) {
    	final Pokemon pokemon = new Pokemon();
    	Visitor visitor = new PokemonVisitor();
    	
    	for(int i = 1; i <= 3; i++) {
    		pokemon.accept(visitor);
    		pokemon.printStage();
    		pokemon.evolve();
    	}
    }
}