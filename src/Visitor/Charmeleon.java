package Visitor;

public class Charmeleon implements State {

	private int bonusPoints;
	public int getBonusPoints() {
		return bonusPoints;
	}
	public void setBonusPoints(int bonusPoints) {
		this.bonusPoints = bonusPoints;
	}
	
	private Charmeleon() {}
	private static final Charmeleon instance = new Charmeleon();
	public static State getInstance() {
		return instance;
	}
	
	public void evolve(final Pokemon pokemon) {
		pokemon.setState(Charizard.getInstance());
	}

	public void printStage() {
		System.out.println("I am Charmeleon and I have "+bonusPoints+" bonus points!");
	}
	
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}
}
