package ChainOfResponsibility;

public class DepartmentManager extends RaiseProcessor {

	public void processRequest(int raisePercentage) {
		if(raisePercentage <= 2) {
			System.out.println("Department manager: 'I can handle this, it's at or below 2%'");
		}
		else {
			System.out.println("Department manager: 'Better send this higher up, it's over 2%'");
			successor.processRequest(raisePercentage);
		}
	}
}
