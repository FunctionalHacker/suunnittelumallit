package ChainOfResponsibility;

public class CEO extends RaiseProcessor {

	public void processRequest(int RaisePercentage) {
		System.out.println("CEO: 'I can handle this, I'm the CEO for Christ's sake!'");
	}
}
