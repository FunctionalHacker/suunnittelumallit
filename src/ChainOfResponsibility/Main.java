package ChainOfResponsibility;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DepartmentManager dm = new DepartmentManager();
		StoreManager sm = new StoreManager();
		CEO ceo = new CEO();
		
		dm.setSuccessor(sm);
		sm.setSuccessor(ceo);
		int raisePercentage;
		
		while(true) {
			System.out.print("Input the raise percentage you want: ");
			raisePercentage = sc.nextInt();
			dm.processRequest(raisePercentage);
			System.out.println(" ");
		}
	}
}
