package ChainOfResponsibility;

public abstract class RaiseProcessor {
	protected RaiseProcessor successor;
	
	public void setSuccessor(RaiseProcessor successor) {
		this.successor = successor;
	}
	
	abstract public void processRequest(int raisePercentage);
}
