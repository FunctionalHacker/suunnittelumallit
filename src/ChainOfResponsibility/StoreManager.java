package ChainOfResponsibility;

public class StoreManager extends RaiseProcessor {

	public void processRequest(int raisePercentage) {
		if(raisePercentage <= 5) {
			System.out.println("Store manager: 'I can handle this, it's at or below 5%'");
		}
		else {
			System.out.println("Store manager: 'Better send this higher up, it's over 5%'");
			successor.processRequest(raisePercentage);
		}
	}
}
