package Facade;

import java.io.UnsupportedEncodingException;

public class RAM {
	private char[] chars;
	public char[] getLoaded() {
		return chars;
	}
	
    public char[] load(int start, int end, byte[] data) {
    	byte[] loadedData = new byte[2048];
    	for(int i = start; i <= end; i++) {
    		loadedData[i] = data[i];
    	}
    	String text = null;
		try {
			text = new String(loadedData, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
    	chars = text.toCharArray();
    	return chars;
    }
}
