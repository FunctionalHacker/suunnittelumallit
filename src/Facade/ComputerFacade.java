package Facade;

public class ComputerFacade {
    private CPU cpu;
    private RAM ram;
    private HDD hdd;

    public ComputerFacade() {
        this.cpu = new CPU();
        this.ram = new RAM();
        this.hdd = new HDD();
    }
    
    public void start() {
    	int position = 0;
    	cpu.jump(position);
        cpu.execute(ram.load(0, 20, hdd.read("boot.cfg")));
        
        for(int i = position; i < 20; i++) {
        	cpu.jump(i);
        	cpu.execute(ram.getLoaded());
        }
    }
}
