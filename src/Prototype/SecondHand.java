package Prototype;

public class SecondHand extends Prototype implements ClockHand {
	private int value;
	
	public SecondHand clone() {
		SecondHand secondHandClone = null;
		try {
			secondHandClone = (SecondHand)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return secondHandClone;
	}
	
	public SecondHand() {
		value = 0;
	}

	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

	public void tick() {
		if(value >= 59) {
			value = 0;
		}
		else {
			value++;
		}
	}
}
