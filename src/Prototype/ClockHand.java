package Prototype;

public interface ClockHand {
	int getValue();
	void setValue(int value);
	void tick();
}
