# Prototype
Kirjoita kello+viisarit -ohjelma, jossa kello koostuu viisariolioista ja selvitä sen avulla, kuinka Javassa toteutetaan Prototypemallia (Object.clone()). Esitä sellainen esimerkki, jossa toteutuu syväkopiointi. Eli: tee kellosta klooni ja katso kuinka se käyttäytyy suhteessa alkuperäiseen, jos muutat alkuperäisen tai kloonin kellonaikaa.
---
Toteuttamani ohjelma tulostaa tämän
```
Using shallow clone

Original clock time: 00:00:00
Shallow cloned clock time: 00:00:00

Changing original clock time

Original clock time: 02:49:02
Shallow cloned clock time: 02:49:02

Changing cloned clock time

Original clock time: 23:22:15
Shallow cloned clock time: 23:22:15

Using deep clone

Original clock time: 00:00:00
Deep cloned clock time: 00:00:00

Changing original clock time

Original clock time: 02:49:02
Deep cloned clock time: 00:00:00

Changing cloned clock time

Original clock time: 02:49:02
Deep cloned clock time: 23:22:15
```
Kelloluokka tallettaa ja hakee tunti- sekunti- ja minuuttiarvonsa viisariolioista. Shallow clonesta voimme päätellä sen, että vaikka kloonattu kello on oma olionsa, sen käyttämät viisarioliot eivät kloonaudu. Eli kloonattu kello viittaa samoihin viisariolioihin kuin mihin alkuperäinenkin kello viittaa, joten kun jommankumman aikaa vaihtaa, molempien ajat vaihtuvat.

Deep cloneen vaihdettaessa huomataan, että aikoja voi vaihtaa itsenäisesti. Deep clonea käytettäessä myös kloonattavan luokan käyttämät luokat kloonataan, joten arvot ovat tallessa omassa paikassaan.
