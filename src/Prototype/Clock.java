package Prototype;

public class Clock extends Prototype {
	private HourHand hourHand;
	private MinuteHand minuteHand;
	private SecondHand secondHand;
	
	public Clock shallowClone() {
		Clock clockClone = null;
		try {
			clockClone = (Clock)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return clockClone;
	}
	
	public Clock deepClone() {
		Clock clockClone = null;
		try {
			clockClone = (Clock)super.clone();
			clockClone.hourHand = (HourHand)hourHand.clone();
			clockClone.minuteHand = (MinuteHand)minuteHand.clone();
			clockClone.secondHand = (SecondHand)secondHand.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return clockClone;
	}
	
	public Clock() {
		hourHand = new HourHand();
		minuteHand = new MinuteHand();
		secondHand = new SecondHand();
	}
	
	public void tick() {
		
		if(secondHand.getValue() == 59) {
			minuteHand.tick();
		}
		if(minuteHand.getValue() == 59) {
			hourHand.tick();
		}
		secondHand.tick();
	}
	
	public void setTime(int hour, int minute, int second) {
		hourHand.setValue(hour);
		minuteHand.setValue(minute);
		secondHand.setValue(second);
	}
	
	public void printTime() {
		String hour = String.valueOf(hourHand.getValue());
		String minute = String.valueOf(minuteHand.getValue());
		String second = String.valueOf(secondHand.getValue());
		
		if(hourHand.getValue() < 10) {
			hour = "0"+hour;
		}
		
		if(minuteHand.getValue() < 10) {
			minute = "0"+minute;
		}
		
		if(secondHand.getValue() < 10) {
			second = "0"+second;
		}
		
		System.out.println(hour+":"+minute+":"+second);
	}
}
