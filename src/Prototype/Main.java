package Prototype;

public class Main {
	
	public static void main(String[] args) throws CloneNotSupportedException {
		// SHALLOW CLONE
		System.out.println("Using shallow clone");
		System.out.println();
		Clock clock = new Clock();
		clock.setTime(0, 0, 0);
		System.out.print("Original clock time: ");
		clock.printTime();
		
		Clock shallowClone = clock.shallowClone();
		System.out.print("Shallow cloned clock time: ");
		shallowClone.printTime();
		
		System.out.println();
		System.out.print("Changing original clock time");
		clock.setTime(2, 49, 02);
		System.out.println();
		System.out.println();

		
		System.out.print("Original clock time: ");
		clock.printTime();
		System.out.print("Shallow cloned clock time: ");
		shallowClone.printTime();
		
		System.out.println();
		System.out.print("Changing cloned clock time");
		shallowClone.setTime(23, 22, 15);
		System.out.println();
		System.out.println();

		System.out.print("Original clock time: ");
		clock.printTime();
		System.out.print("Shallow cloned clock time: ");
		shallowClone.printTime();
		
		// DEEP CLONE
		System.out.println();
		System.out.println("Using deep clone");
		System.out.println();
		
		clock.setTime(0, 0, 0);
		System.out.print("Original clock time: ");
		clock.printTime();
		
		Clock deepClone = clock.deepClone();
		System.out.print("Deep cloned clock time: ");
		deepClone.printTime();
		
		System.out.println();
		System.out.print("Changing original clock time");
		clock.setTime(2, 49, 02);
		System.out.println();
		System.out.println();

		
		System.out.print("Original clock time: ");
		clock.printTime();
		System.out.print("Deep cloned clock time: ");
		deepClone.printTime();
		
		System.out.println();
		System.out.print("Changing cloned clock time");
		deepClone.setTime(23, 22, 15);
		System.out.println();
		System.out.println();

		System.out.print("Original clock time: ");
		clock.printTime();
		System.out.print("Deep cloned clock time: ");
		deepClone.printTime();
		
//		while(true) {
//			for (int i = 0; i < 50; ++i) System.out.println();
//			System.out.print("Clock: ");
//			clone.printTime();
//			clone.tick();
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//		}
	}
}
