package Prototype;

public class HourHand extends Prototype implements ClockHand {
	private int value;
	
	public HourHand clone() {
		HourHand hourHandClone = null;
		try {
			hourHandClone = (HourHand)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return hourHandClone;
	}
	
	public HourHand() {
		value = 0;
	}

	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

	public void tick() {
		if(value >= 23) {
			value = 0;
		}
		else {
			value++;
		}
	}
}
