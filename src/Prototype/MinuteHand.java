package Prototype;

public class MinuteHand extends Prototype implements ClockHand {
	private int value;
	
	public MinuteHand clone() {
		MinuteHand minuteHandClone = null;
		try {
			minuteHandClone = (MinuteHand)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return minuteHandClone;
	}
	
	public MinuteHand() {
		value = 0;
	}

	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

	public void tick() {
		if(value >= 59) {
			value = 0;
		}
		else {
			value++;
		}
	}
}
