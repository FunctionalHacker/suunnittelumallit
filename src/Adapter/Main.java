package Adapter;

public class Main {
	public static void main(String[] args) {
		SocketAdapter sa = new MultiAdapter();
		Volt v3 = sa.get3Volt();
		Volt v12 = sa.get12Volt();
		Volt v120 = sa.get120Volt();
		
		System.out.println("From the adapter we get: "+v3.getVolts()+" volts, "
							+v12.getVolts()+" volts and "
							+v120.getVolts()+" volts");
	}
}
