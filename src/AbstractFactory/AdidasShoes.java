package AbstractFactory;

public class AdidasShoes implements PieceOfClothing {
	public String toString() {
		return "Adidas sneakers";
	}
}
