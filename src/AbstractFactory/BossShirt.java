package AbstractFactory;

public class BossShirt implements PieceOfClothing {
	public String toString() {
		return "a Boss button shirt";
	}
}
