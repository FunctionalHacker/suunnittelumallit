package AbstractFactory;

public interface ClothingFactory {
	public PieceOfClothing getShoes();
	public PieceOfClothing getPants();
	public PieceOfClothing getShirt();
	public PieceOfClothing getHat();
}
