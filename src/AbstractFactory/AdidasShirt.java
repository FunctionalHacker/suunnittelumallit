package AbstractFactory;

public class AdidasShirt implements PieceOfClothing {
	public String toString() {
		return "an Adidas t-shirt";
	}
}
