package AbstractFactory;

public class AdidasHat implements PieceOfClothing {
	public String toString() {
		return "an Adidas cap";
	}
}
