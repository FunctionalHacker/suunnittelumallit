package AbstractFactory;

public class Jasper implements Student {
	ClothingFactory clothingFactory;
	PieceOfClothing shoes;
	PieceOfClothing pants;
	PieceOfClothing shirt;
	PieceOfClothing hat;
	
	public Jasper() {
		clothingFactory = new AdidasFactory();
		getDressed();
	}
	
	public void whatImWearing() {
		System.out.println("I'm wearing "+shoes+", "+pants+", "+shirt+" and "+hat);
	}
	
	public void getDressed() {
		shoes = clothingFactory.getShoes();
		pants = clothingFactory.getPants();
		shirt = clothingFactory.getShirt();
		hat = clothingFactory.getHat();
	}
	
	public void graduate() {
		clothingFactory = new BossFactory();
		getDressed();
	}
	
}
