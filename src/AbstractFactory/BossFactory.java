package AbstractFactory;

public class BossFactory implements ClothingFactory {

	public PieceOfClothing getShoes() {
		return new BossShoes();
	}

	public PieceOfClothing getPants() {
		return new BossPants();
	}

	public PieceOfClothing getShirt() {
		return new BossShirt();
	}

	public PieceOfClothing getHat() {
		return new BossHat();
	}
}
