# Flyweight
_Lue Flyweight-mallia käsittelevä [artikkeli](https://www.javaworld.com/article/2073632/build-ci-sdlc/make-your-apps-fly.html). Testaa esimerkit 1 ja 2: String ja Border. Valmistaudu esittelemään, mitä testien perusteella voidaan todeta. Palauta raportti OMAan._
---

## Esimerkki 1: String
```java
public class StringTest {
   public static void main(String[] args) {
      String fly = "fly", weight = "weight";
      String fly2 = "fly", weight2 = "weight"; 
      System.out.println(fly == fly2);       // fly and fly2 refer to the same String instance
      System.out.println(weight == weight2); // weight and weight2 also refer to
                                             // the same String instance
      String distinctString = fly + weight;
      System.out.println(distinctString == "flyweight"); // flyweight and "flyweight" do not
                                                         // refer to the same instance
      String flyweight = (fly + weight).intern();
      System.out.println(flyweight == "flyweight"); // The intern() method returns a flyweight
   }
}
```
Output:
```
true
true
false
true
```
Tämä esimerkki havainnollistaa Javan rankkaa optimointia merkkijonojen suhteen. Merkkijonoja onkin ohjelmissa paljon, joten niitä kannattaakin optimoida paremman suorituskyvyn takia. Esimerkissä on alussa kaksi muuttujaa molemmille merkkijonoille "fly" ja "weight". Huomataan, että fly ja fly2 -merkkijonot viittaavat samaan olioon (`==` operaattori palauttaa true jos oliot ovat samat). Tästä siis nähdään, että Java käyttää itsessäänkin jo Flyweight-mallia.

## Esimerkki 2: Border
```
package Flyweight;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
@SuppressWarnings("serial")
public class Test extends JFrame {

	@SuppressWarnings("deprecation")
   public static void main(String[] args) {
      Test test = new Test();
      test.setBounds(20,20,250,150);
      test.show();
   }
   public Test() {
      super("Border flyweights");
      JPanel panel   = new JPanel(), panel2 = new JPanel();
      Border border  = BorderFactory.createRaisedBevelBorder();
      Border border2 = BorderFactory.createRaisedBevelBorder();
      panel.setBorder(border);
      panel.setPreferredSize(new Dimension(100,100));
      panel2.setBorder(border2);
      panel2.setPreferredSize(new Dimension(100,100));
      Container contentPane = getContentPane();
      contentPane.setLayout(new FlowLayout());
      contentPane.add(panel);
      contentPane.add(panel2);
      setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      addWindowListener(new WindowAdapter() {
         public void windowClosed(WindowEvent e) {
            System.exit(0);
         }
      });
      if(border == border2)
         System.out.println("bevel borders are shared");
      else
         System.out.println("bevel borders are NOT shared");
   }
}
```
Output:
```
bevel borders are shared
```
Tässä esimerkissä liitetään kahteen JPanel-olioon reunaolioita. Tulostuksesta huomataan, että molemmat reunaoliot viittaavat samaan olioon, joten Java on taas käyttänyt Flyweight-mallia ja optimoinut koodia. Koska molemmat reunaoliot ovat samassa paikassa, ne voivat yhtä hyvin olla sama olio.
