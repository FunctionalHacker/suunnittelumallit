package Builder;

public abstract class BurgerBuilder {
	protected Burger burger;
	public Burger getBurger() {return burger;}
	
	public abstract void createNewBurger();
	public abstract void buildBun();
	public abstract void buildPatty();
	public abstract void buildSauce();
	public abstract void buildCheese();
}
