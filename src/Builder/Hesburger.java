package Builder;

public class Hesburger extends BurgerBuilder {

	public void buildBun() {burger.setBun("Bun without seeds");}

	public void buildPatty() {burger.setPatty("1x 300g patty");}

	public void buildSauce() {burger.setSauce("Famous Hesburger mayonnaise");}

	public void buildCheese() {burger.setCheese("Monterey Jack");}

	public void createNewBurger() {
		this.burger = new Burger();
	}
}
