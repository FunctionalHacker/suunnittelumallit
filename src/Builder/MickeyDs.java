package Builder;

import java.util.List;

public class MickeyDs extends BurgerBuilder {

	public void buildBun() {burger.addIngredient("Bun with seeds");}

	public void buildPatty() {burger.addIngredient("3x 100g patty");}

	public void buildSauce() {burger.addIngredient("Ketchup");}

	public void buildCheese() {burger.addIngredient("Cheddar");}

	public void createNewBurger() {
		this.burger = new Burger(true);
	}
}
