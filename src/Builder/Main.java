package Builder;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		MickeyDs md = new MickeyDs();
		md.createNewBurger();
		md.buildBun();
		md.buildPatty();
		md.buildSauce();
		md.buildCheese();
		Burger mdBurger = md.getBurger();

		Hesburger hb = new Hesburger();
		hb.createNewBurger();
		hb.buildBun();
		hb.buildPatty();
		hb.buildSauce();
		hb.buildCheese();
		Burger hbBurger = hb.getBurger();
		
		String bun, patty, sauce, cheese;
		ArrayList<String>ingredients = mdBurger.getIngredients();
		bun = hbBurger.getBun();
		patty = hbBurger.getPatty();
		sauce = hbBurger.getSauce();
		cheese = hbBurger.getCheese();
				
		System.out.println("Mickey D's burger contains:");
		for (int i = 0; i < ingredients.size(); i++) {
			System.out.println(ingredients.get(i));
		}
		
		System.out.println(" ");
		
		System.out.println("Hesburger burger contains:");
		System.out.println(hbBurger.getBun());
		System.out.println(hbBurger.getPatty());
		System.out.println(hbBurger.getSauce());
		System.out.println(hbBurger.getCheese());
	}
}
