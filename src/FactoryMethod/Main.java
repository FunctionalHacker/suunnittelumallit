package FactoryMethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus opiskelija = new Opiskelija();
        AterioivaOtus talonmies = new Talonmies();
        
        opettaja.aterioi();
        System.out.println(" ");
        opiskelija.aterioi();
        System.out.println(" ");
        talonmies.aterioi();
    }
}
