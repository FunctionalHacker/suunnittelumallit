# Factory Method
Työtilan Tehtäväpohjat-kansiossa on factorymethod.zip (NetBeans-projekti), joka sisältää koodia, jossa AterioivaOtus aterioi.

Esimerkkiohjelman Juoma luodaan AterioivanOtuksen Opettajaaliluokassa. Juomanluontimetodi on tehdasmetodi. Kirjoita Opettajaluokalle kaksi rinnakkaista luokkaa, joissa kummassakin luodaan sopiva juoma. Luo testiohjelmassasi jokaista otustyyppiä oleva olio ja laita ne aterioimaan.
