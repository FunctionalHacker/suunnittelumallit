package Composite;

public interface ComputerComponent {
	public double getPrice();
	public String getName();
}
