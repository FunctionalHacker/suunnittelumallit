package Composite;

public class Computer {
	ComputerComponent motherboard;
	ComputerComponent powerSupply;
	ComputerComponent ram;
	ComputerComponent cpu;
	ComputerComponent gpu;
	ComputerComponent storage;
	ComputerComponent computerCase;
	ComputerComponent operatingSystem;

	public double getPrice() {
		double total = 	motherboard.getPrice()
						+powerSupply.getPrice()
						+ram.getPrice()
						+cpu.getPrice()
						+gpu.getPrice()
						+storage.getPrice()
						+operatingSystem.getPrice();
		return total;
	}
	
	public void printConfig() {
			System.out.println("Motherboard:      "+motherboard.getName()+", price: "+motherboard.getPrice()+"€");
			System.out.println("Power supply:     "+powerSupply.getName()+", price: "+powerSupply.getPrice()+"€");
			System.out.println("RAM:              "+ram.getName()+", price: "+ram.getPrice()+"€");
			System.out.println("CPU:              "+cpu.getName()+", price: "+cpu.getPrice()+"€");
			System.out.println("GPU:              "+gpu.getName()+", price: "+gpu.getPrice()+"€");
			System.out.println("Storage:          "+storage.getName()+", price: "+storage.getPrice()+"€");
			System.out.println("Operating system: "+operatingSystem.getName()+", price: "+operatingSystem.getPrice()+"€");
			System.out.println("Total price:      "+getPrice()+"€");
	}
	
	public void setMotherboard(ComputerComponent motherboard) {
		this.motherboard = motherboard;
	}

	public void setPowerSupply(ComputerComponent powerSupply) {
		this.powerSupply = powerSupply;
	}

	public void setRAM(ComputerComponent ram) {
		this.ram = ram;
	}

	public void setCPU(ComputerComponent cpu) {
		this.cpu = cpu;
	}

	public void setGPU(ComputerComponent gpu) {
		this.gpu = gpu;
	}

	public void setStorage(ComputerComponent storage) {
		this.storage = storage;
	}
	
	public void setCase(ComputerComponent computerCase) {
		this.computerCase = computerCase;
	}

	public void setOperatingSystem(ComputerComponent operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
}
