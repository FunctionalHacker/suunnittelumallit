package Composite;

public class OperatingSystem implements ComputerComponent {
	
	private double price;
	private String name;
	
	public OperatingSystem(String name, double price) {
		this.price = price;
		this.name = name;
	}

	public double getPrice() {
		return price;
	}
	
	public String getName() {
		return name;
	}
}