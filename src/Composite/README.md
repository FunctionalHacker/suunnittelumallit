# Composite
Suunnittele Composite-mallin mukaisesti rajapinnat ja toteutukset systeemille, jolla voit esittää pöytätietokoneen kokoonpanon ja laskea sen hinnan, kun kukin komponentti tietää oman hintansa. Idea on että tietokoneen kaikki osat ovat vaikkapa Laiteosa-rajapinnan toteuttajia. Laiteosalla on hinta.

Laiteosia on erilaisia, esim:
- muistipiiri
- emolevy
- prosessori
- verkkokortti
- näytönohjain
- kotelo
