package Composite;

public class Main {
	public static void main(String[] args) {
		Computer pc = new Computer();
		ComputerComponent mb = new Motherboard("MSI B350M MORTAR ARCTIC mATX", 99.9);
		ComputerComponent psu = new PowerSUpply("Silverstone 600W Strider SFX", 116);
		ComputerComponent ram = new RAM("Kingston 64GB (4x16GB) Hyper X Fury DDR4", 729);
		ComputerComponent cpu = new CPU("AMD Ryzen 7 2700", 304.9);
		ComputerComponent gpu = new GPU("Sapphire RX Vega 64 NITRO+", 839);
		ComputerComponent cs = new Case("BitFenix Prodigy M Arctic White mATX", 85.9);
		ComputerComponent storage = new Storage("Kingston 960GB HyperX Predator PCI-E 1400/1000 MB/s", 579);
		ComputerComponent os = new OperatingSystem("Arch Linux", 0);
		
		pc.setMotherboard(mb);
		pc.setPowerSupply(psu);
		pc.setRAM(ram);
		pc.setCPU(cpu);
		pc.setGPU(gpu);
		pc.setStorage(storage);
		pc.setCase(cs);
		pc.setOperatingSystem(os);
		
		pc.printConfig();
	}
}
