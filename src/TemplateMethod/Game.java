package TemplateMethod;

public abstract class Game {
	abstract void initializeGame();
	abstract void makePlay();
	abstract boolean endOfGame();
	abstract void printWinner();
	
	/* The template method : */
	public final void playOneGame() {
		initializeGame();
		while (!endOfGame()){
			makePlay();
		}
		printWinner();
	}
}
