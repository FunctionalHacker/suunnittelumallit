package TemplateMethod;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors extends Game {
	Random rd;
	Scanner sc;
	String playerName;
	String playerPlay;
	String computerPlay;
	int computerInt;
	int winner;

	void initializeGame() {
		rd = new Random();
		sc = new Scanner(System.in);
		winner = -1;
		System.out.println("Welcome, let's play Rock, Paper, Scissors!");
		System.out.println();
		System.out.print("What's your name: ");
		playerName = sc.nextLine();
		System.out.println();
	}
	
    void makePlay() {
    	System.out.print("Rock, paper or scissors?[r p s]: ");
    	playerPlay = sc.next();
    	computerInt = rd.nextInt(3 - 1 + 1) + 1;
    	
    	switch (computerInt) {
    		case 1: System.out.println("Computer played rock");
    				computerPlay = "r";
    				break;
    		case 2: System.out.println("Computer played paper");	
    				computerPlay = "p";
    				break;
    		case 3:	System.out.println("Computer played scissors");
    				computerPlay = "s";
    				break;
    	}
    	
    	if(playerPlay.equals(computerPlay)) {
    		winner = 1;
    	}
    	else if(playerPlay.equals("r")) {
    		if(computerPlay.equals("p")) {
    			winner = 0;
    		}
    		else if(computerPlay.equals("s")) {
    			winner = 2;
    		}
    	}
    	else if(playerPlay.equals("p")) {
    		if(computerPlay.equals("r")) {
    			winner = 2;
    		}
    		else if(computerPlay.equals("s")) {
    			winner = 0;
    		}
    	}
    	else if(playerPlay.equals("s")) {
    		if(computerPlay.equals("r")) {
    			winner = 0;
    		}
    		else if(computerPlay.equals("p")) {
    			winner = 2;
    		}
    	}
    }
    
    boolean endOfGame() {
    	if(winner >= 0 && winner <= 2) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    void printWinner() {
    	switch(winner) {
    		case 0: System.out.println("And the winner is the computer!");
    				break;
    		case 1: System.out.println("It's a tie!");
    				break;
    		case 2: System.out.println("And the winner is "+playerName);
    				break;
    	}
    }
}
