package TemplateMethod;

public class Main {

	public static void main(String[] args) {
		Game rps = new RockPaperScissors();
		rps.playOneGame();
	}
}
