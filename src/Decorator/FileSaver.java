package Decorator;

public interface FileSaver {
	public void saveFile(byte[] text);
	public byte[] readFile();
}
