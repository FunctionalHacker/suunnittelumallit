package Decorator;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class SimpleFileSaver implements FileSaver {
	Scanner sc = new Scanner(System.in);

	public void saveFile(byte[] text) {
		System.out.println("Saving file to disk");
		try {
			Files.write(Paths.get("simplefile.txt"), text);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public byte[] readFile() {
		System.out.println("Reading file from disk");
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get("simplefile.txt"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encoded;
	}
}
