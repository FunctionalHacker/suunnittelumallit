package Command;

public class ScreenDownCommand implements Command {
	private ProjectorScreen screen;
	
	public ScreenDownCommand(ProjectorScreen screen) {
		this.screen = screen;
	}

	@Override
	public void execute() {
		screen.down();
	}
}
