package Command;

public class ScreenUpCommand implements Command {
	private ProjectorScreen screen;
	
	public ScreenUpCommand(ProjectorScreen screen) {
		this.screen = screen;
	}

	@Override
	public void execute() {
		screen.up();
	}
}
